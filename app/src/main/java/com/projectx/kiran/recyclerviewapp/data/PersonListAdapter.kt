package com.projectx.kiran.recyclerviewapp.data

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.projectx.kiran.recyclerviewapp.R
import com.projectx.kiran.recyclerviewapp.model.Person

/**
 * Created by kiran on 4/13/2018.
 */

class PersonListAdapter (private val list: ArrayList<Person>,
                         private val context: Context) : RecyclerView.Adapter<PersonListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder?.bindItem(list[position])

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, position: Int): ViewHolder {
// create view from xml file list_row
        val view = LayoutInflater.from(context).inflate(R.layout.list_row, parent, false)
        return ViewHolder(view, context)
    }

    inner class ViewHolder(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView){
        fun bindItem(person: Person){
            var name:TextView = itemView.findViewById(R.id.name)
            var  age:TextView = itemView.findViewById(R.id.age)

            name.text = person.name
            age.text = person.age.toString()

            itemView.setOnClickListener{
                Toast.makeText(context, name.text, Toast.LENGTH_SHORT).show()
            }

        }
    }

}